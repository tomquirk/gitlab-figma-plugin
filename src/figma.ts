import MESSAGE_TYPES from './message_types';

figma.showUI(__html__);

function handleUploadDesign({ payload }): void {
  figma.currentPage.exportAsync().then(data => {
    figma.ui.postMessage({
      type: MESSAGE_TYPES.UPLOAD_DESIGN,
      payload: {
        ...payload,
        data,
      },
    });
  });
}

figma.ui.onmessage = async (event): Promise<void> => {
  if (event.type === MESSAGE_TYPES.UPLOAD_DESIGN) {
    handleUploadDesign(event);
  }

  if (event.type === MESSAGE_TYPES.UPLOAD_DESIGN_DONE) {
    figma.closePlugin();
  }
};
