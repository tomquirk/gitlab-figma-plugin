import './index.css';
import createClient from './apollo_client';
import uploadDesignMutation from './graphql/uploadDesign.mutation.graphql';
import MESSAGE_TYPES from '../message_types';

const exportForm = document.getElementById('gitlab_export_form');
const tokenEl = document.getElementById('gitlab_export_token') as HTMLInputElement;
const projectPathEl = document.getElementById('gitlab_export_project_path') as HTMLInputElement;
const issueIdEl = document.getElementById('gitlab_export_iid') as HTMLInputElement;
const filenameEl = document.getElementById('gitlab_export_filename') as HTMLInputElement;

function uploadDesign(
  token: string,
  projectPath: string,
  issueId: string,
  files: File[],
): Promise<any> {
  const ApolloClient = createClient(token);

  const mutationPayload = {
    variables: {
      files,
      projectPath,
      iid: issueId,
    },
    context: {
      hasUpload: true,
    },
    mutation: uploadDesignMutation,
  };

  return ApolloClient.mutate(mutationPayload);
}

function handleUploadDesignEvent(event): void {
  const { token, projectPath, issueId, data, filename } = event.data.pluginMessage.payload;
  const fileData = new Blob([data], { type: 'image/png' });
  const file = new File([fileData], filename, {
    type: 'image/png',
  });

  uploadDesign(token, projectPath, issueId, [file])
    .then(res => {
      window.parent.postMessage(
        { pluginMessage: { type: MESSAGE_TYPES.UPLOAD_DESIGN_DONE, payload: res } },
        '*',
      );
    })
    .catch(err => {
      console.log('err', err);
    });
}

window.onmessage = async (event): Promise<void> => {
  if (event.data.pluginMessage.type === MESSAGE_TYPES.UPLOAD_DESIGN) {
    handleUploadDesignEvent(event);
  }
};

exportForm.addEventListener('submit', ev => {
  ev.preventDefault();

  const token = tokenEl.value;
  const projectPath = projectPathEl.value;
  const issueId = issueIdEl.value;
  const filename = filenameEl.value;
  const payload = { token, projectPath, issueId, filename };

  window.parent.postMessage({ pluginMessage: { type: MESSAGE_TYPES.UPLOAD_DESIGN, payload } }, '*');
});
