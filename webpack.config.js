const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');

module.exports = {
  entry: {
    figma: './src/figma.ts',
    index: './src/plugin_ui/index.ts',
  },
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    port: 9000,
    writeToDisk: true,
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.(graphql|gql)$/,
        exclude: /node_modules/,
        loader: 'graphql-tag/loader',
      },
      { test: /\.css$/, loader: [{ loader: 'style-loader' }, { loader: 'css-loader' }] },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/plugin_ui/index.html',
      inlineSource: '.(js)$', // embed all javascript and css inline
      excludeChunks: ['figma'], // exclude style.js or style.[chunkhash].js
    }),
    new HtmlWebpackInlineSourcePlugin(),
  ],
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist'),
  },
};
