# GitLab Figma Plugin (unofficial)

## ⚠️ DEPRECATED: Get the [Official GitLab Figma Plugin Here](https://gitlab.com/gitlab-org/gitlab-figma-plugin)

🎨 A GitLab integration for Figma

### Getting started (development)

1. Install dependencies

   ```bash
   npm install
   ```

2. Build assets

   ```bash
   npm run compile
   ```

#### Figma setup

Now that we've compiled the plugin, we need to add it to Figma.

1. In the hamburger menu top-left, navigate to _Plugins/Development/New plugin..._
2. Select this projects' `manifest.json`
3. Follow prompts to finish plugin creation

### Usage

Currently, the plugin only supports uploading of a design to a given Issue within GitLab.

#### Exporting a Figma Design to GitLab

1. Run the plugin (In the hamburger menu top-left, click _Plugins/Development/\<GitLab Plugin Name\>..._)
